package com.fintech_school.currency_trader.repo.interfaces;

import com.fintech_school.currency_trader.data.Filter;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface FilterDataSource {

    Single<Filter> getFilter();
    Completable saveFilter(Filter filter);
}
