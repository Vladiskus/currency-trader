package com.fintech_school.currency_trader.repo;

import com.fintech_school.currency_trader.data.Currency;
import com.fintech_school.currency_trader.data.Filter;
import com.fintech_school.currency_trader.data.HistoricalData;
import com.fintech_school.currency_trader.data.Transaction;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class LocalDataSource {

    private MainDatabase database;

    @Inject
    public LocalDataSource(MainDatabase database) {
        this.database = database;
    }

    public Flowable<List<Currency>> getCurrencies(List<String> names) {
        return (names == null ? database.getCurrencyDao().getCurrencies() :
                database.getCurrencyDao().getSpecificCurrencies(names))
                .throttleWithTimeout(400, TimeUnit.MILLISECONDS)
                .map(currencies -> {
                    Collections.sort(currencies, Collections.reverseOrder());
                    return currencies;
                });
    }

    public Completable saveCurrencies(Single<List<Currency>> source) {
        return source.map(currencies -> {
            database.getCurrencyDao().addCurrencies(currencies);
            for (Currency currency : currencies)
                database.getCurrencyDao().updateCurrencyValue(currency.getName(), currency.getValue());
            return currencies;
        }).toCompletable();
    }

    public Completable updateCurrency(Currency currency) {
        return Completable.fromAction(() -> database.getCurrencyDao().updateCurrency(currency));
    }

    public Flowable<List<Transaction>> getTransactions() {
        return database.getTransactionDao().getTransactions();
    }

    public Completable saveTransaction(Transaction transaction) {
        return Completable.fromAction(() -> database.getTransactionDao().addTransaction(transaction));
    }

    public Completable saveFilter(Filter filter) {
        return Completable.fromAction(() -> database.getFilterDao().saveFilter(filter));
    }

    public Single<Filter> getFilter() {
        return database.getFilterDao().getFilter().toSingle(new Filter(Filter.Period.ALL_TIME));
    }

    public Flowable<List<HistoricalData>> getHistoricalData(String currency, List<Date> dates) {
        return database.getHistoricalDataDao().getHistoricalData(currency, dates).map(data -> {
                    Collections.sort(data);
                    return data;
                }
        );
    }

    public Completable saveHistoricalData(Single<List<HistoricalData>> source) {
        return source.map(historicalData -> {
            database.getHistoricalDataDao().saveHistoricalData(historicalData);
            return historicalData;
        }).toCompletable();
    }
}