package com.fintech_school.currency_trader.repo;

import com.fintech_school.currency_trader.data.Currency;
import com.fintech_school.currency_trader.data.HistoricalData;
import com.fintech_school.currency_trader.util.DateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class RemoteDataSource {

    private WebService webService;

    @Inject
    public RemoteDataSource(WebService webService) {
        this.webService = webService;
    }

    public Single<List<Currency>> downloadCurrencyValues() {
        return webService.downloadCurrencies().firstOrError();
    }

    public Single<List<HistoricalData>> downloadHistoricalData(String currency, List<Date> dates) {
        return Single.create(emitter -> {
            List<HistoricalData> data = new ArrayList<>();
            for (Date date : dates) {
                webService.downloadHistoricalData(DateUtil.getString(date, "yyyy-MM-dd"), currency)
                        .firstOrError()
                        .subscribe(historicalData -> {
                            historicalData.setDate(date);
                            data.add(historicalData);
                            if (data.size() == dates.size()) emitter.onSuccess(data);
                        }, emitter::onError);
            }
        });
    }
}