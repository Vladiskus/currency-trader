package com.fintech_school.currency_trader.repo;

import android.support.annotation.Nullable;

import com.fintech_school.currency_trader.data.Currency;
import com.fintech_school.currency_trader.data.Filter;
import com.fintech_school.currency_trader.data.HistoricalData;
import com.fintech_school.currency_trader.data.Transaction;
import com.fintech_school.currency_trader.repo.interfaces.CurrencyDataSource;
import com.fintech_school.currency_trader.repo.interfaces.FilterDataSource;
import com.fintech_school.currency_trader.repo.interfaces.HistoricalDataSource;
import com.fintech_school.currency_trader.repo.interfaces.TransactionDataSource;
import com.fintech_school.currency_trader.util.DateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class MainDataSource implements CurrencyDataSource, TransactionDataSource,
        FilterDataSource, HistoricalDataSource {

    private LocalDataSource localDataSource;
    private RemoteDataSource remoteDataSource;

    @Inject
    public MainDataSource(LocalDataSource localDataSource, RemoteDataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    @Override
    public Observable<List<Currency>> getCurrencies(@Nullable List<String> names) {
        return Observable.create(emitter ->
            emitter.setDisposable(localDataSource.getCurrencies(names)
                    .doOnNext(currencies -> {
                        if (currencies.size() > 1) emitter.onNext(currencies);
                        else localDataSource.saveCurrencies(remoteDataSource.downloadCurrencyValues())
                                    .subscribe(() -> {}, emitter::onError);
                    }).subscribe()));
    }

    @Override
    public Completable refreshCurrencyValues() {
        return remoteDataSource.downloadCurrencyValues().toCompletable();
    }

    @Override
    public Completable updateFavoriteState(Currency currency) {
        currency.setFavorite(!currency.isFavorite());
        return localDataSource.updateCurrency(currency);
    }

    @Override
    public Completable markAsRecent(Currency currency) {
        currency.setMaxIndex();
        return localDataSource.updateCurrency(currency);
    }

    @Override
    public Flowable<List<Transaction>> getTransactions() {
        return localDataSource.getTransactions();
    }

    @Override
    public Completable saveTransaction(Transaction transaction) {
        return localDataSource.saveTransaction(transaction);
    }

    @Override
    public Single<Filter> getFilter() {
        return localDataSource.getFilter();
    }

    @Override
    public Completable saveFilter(Filter filter) {
        return localDataSource.saveFilter(filter);
    }

    @Override
    public Single<List<HistoricalData>> getHistoricalData(String currency, Date startDate, Date endDate) {
        return Single.create(emitter -> {
            List<Date> dates = DateUtil.getDateList(startDate, endDate);
            emitter.setDisposable(localDataSource.getHistoricalData(currency, dates).doOnNext(data -> {
                if (data.size() == dates.size()) emitter.onSuccess(data);
                else {
                    List<Date> missingDates = new ArrayList<>(dates);
                    for (HistoricalData historicalData : data) missingDates.remove(historicalData.getDate());
                    localDataSource.saveHistoricalData(remoteDataSource.downloadHistoricalData(currency, missingDates))
                            .subscribe(() -> {}, emitter::onError);
                }
            }).subscribe());
        });
    }
}