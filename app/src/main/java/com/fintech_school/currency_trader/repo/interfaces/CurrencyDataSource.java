package com.fintech_school.currency_trader.repo.interfaces;

import android.support.annotation.Nullable;

import com.fintech_school.currency_trader.data.Currency;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface CurrencyDataSource {

    Observable<List<Currency>> getCurrencies(@Nullable List<String> names);
    Completable refreshCurrencyValues();
    Completable updateFavoriteState(Currency currency);
    Completable markAsRecent(Currency currency);
}
