package com.fintech_school.currency_trader.repo.interfaces;

import com.fintech_school.currency_trader.data.HistoricalData;

import java.util.Date;
import java.util.List;

import io.reactivex.Single;

public interface HistoricalDataSource {

    Single<List<HistoricalData>> getHistoricalData(String currency, Date startDate, Date endDate);
}
