package com.fintech_school.currency_trader.repo.interfaces;

import com.fintech_school.currency_trader.data.Transaction;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public interface TransactionDataSource {

    Flowable<List<Transaction>> getTransactions();
    Completable saveTransaction(Transaction transaction);
}
