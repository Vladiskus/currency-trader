package com.fintech_school.currency_trader.analytics;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fintech_school.currency_trader.R;
import com.fintech_school.currency_trader.databinding.SimpleCurrencyItemBinding;
import com.fintech_school.currency_trader.parents.adapters.BaseAdapter;
import com.fintech_school.currency_trader.parents.adapters.ClickListener;

import java.util.List;

public class SimpleCurrencyAdapter extends BaseAdapter<String> {

    private boolean isFirstAdapter;
    private String selectedCurrency;

    public SimpleCurrencyAdapter(@Nullable List<String> elements, @Nullable ClickListener listener,
                                 boolean isFirstAdapter, String selectedCurrency) {
        super(elements, listener);
        this.isFirstAdapter = isFirstAdapter;
        this.selectedCurrency = selectedCurrency;
    }

    public void setSelectedCurrency(String selectedCurrency) {
        if (this.selectedCurrency.equals(selectedCurrency)) return;
        int oldPosition = elements.indexOf(this.selectedCurrency);
        this.selectedCurrency = selectedCurrency;
        notifyItemChanged(oldPosition);
        notifyItemChanged(elements.indexOf(selectedCurrency));
    }

    @Override
    public SimpleCurrencyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SimpleCurrencyItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.simple_currency_item, parent, false);
        return new SimpleCurrencyAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bind(elements.get(position), selectedCurrency,
                isFirstAdapter, (OnSimpleCurrencyClickListener) listener);
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        private SimpleCurrencyItemBinding binding;

        private ViewHolder(SimpleCurrencyItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bind(String currency, String selectedCurrency, boolean isFirstAdapter,
                          OnSimpleCurrencyClickListener listener) {
            binding.text.setText(currency);
            binding.text.setBackground(binding.text.getContext().getResources().getDrawable(
                    currency.equals(selectedCurrency) ? R.color.colorAccent : R.color.colorPrimaryLight));
            binding.text.setTextColor(binding.text.getContext().getResources().getColor(
                    currency.equals(selectedCurrency) ? android.R.color.white : android.R.color.black));
            binding.text.setOnClickListener(view -> {
                if (isFirstAdapter) listener.onFirstCurrencyClick(currency);
                else listener.onSecondCurrencyClick(currency);
            });
        }
    }
}