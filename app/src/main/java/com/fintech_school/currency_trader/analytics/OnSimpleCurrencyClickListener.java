package com.fintech_school.currency_trader.analytics;

import com.fintech_school.currency_trader.parents.adapters.ClickListener;

public interface OnSimpleCurrencyClickListener extends ClickListener{

    void onFirstCurrencyClick(String currency);
    void onSecondCurrencyClick(String currency);
}
