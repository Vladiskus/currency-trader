package com.fintech_school.currency_trader.history.transaction_list_screen;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fintech_school.currency_trader.R;
import com.fintech_school.currency_trader.data.Transaction;
import com.fintech_school.currency_trader.databinding.TransactionItemBinding;
import com.fintech_school.currency_trader.parents.adapters.BaseMutableAdapter;
import com.fintech_school.currency_trader.parents.adapters.ClickListener;

public class TransactionAdapter extends BaseMutableAdapter<Transaction> {

    public TransactionAdapter(@Nullable ClickListener listener) {
        super(listener);
    }

    @Override
    protected boolean areItemsTheSame(Transaction first, Transaction second) {
        return first.getDate().getTime() == second.getDate().getTime();
    }

    @Override
    public TransactionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TransactionItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.transaction_item, parent, false);
        return new TransactionAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bind(elements.get(position));
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        private TransactionItemBinding binding;

        private ViewHolder(TransactionItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bind(Transaction transaction) {
            binding.setTransaction(transaction);
        }
    }
}