package com.fintech_school.currency_trader.history.transaction_filter_screen;

import com.fintech_school.currency_trader.parents.adapters.ClickListener;

public interface OnUsedCurrencyClickListener extends ClickListener{

    void onCheckedChange(String currencyName, boolean isChecked);
}
