package com.fintech_school.currency_trader.history.transaction_filter_screen;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fintech_school.currency_trader.R;
import com.fintech_school.currency_trader.data.Filter;
import com.fintech_school.currency_trader.databinding.UsedCurrencyItemBinding;
import com.fintech_school.currency_trader.parents.adapters.BaseAdapter;
import com.fintech_school.currency_trader.parents.adapters.ClickListener;

import java.util.List;

public class UsedCurrencyAdapter extends BaseAdapter<String> {

    private Filter filter;

    public UsedCurrencyAdapter(@Nullable List<String> elements, @Nullable ClickListener listener, Filter filter) {
        super(elements, listener);
        this.filter = filter;
    }

    @Override
    public UsedCurrencyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        UsedCurrencyItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.used_currency_item, parent, false);
        return new UsedCurrencyAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bind(elements.get(position), filter.getSelectedCurrencies()
                .contains(elements.get(position)), (OnUsedCurrencyClickListener) listener);
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        private UsedCurrencyItemBinding binding;

        private ViewHolder(UsedCurrencyItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bind(String currency, boolean isSelected, OnUsedCurrencyClickListener listener) {
            binding.text.setText(currency);
            binding.checkBox.setChecked(isSelected);
            binding.checkBox.setOnCheckedChangeListener((buttonView, isChecked) ->
                    listener.onCheckedChange(currency, isChecked));
        }
    }
}