package com.fintech_school.currency_trader.exchange.currency_list_screen;

import com.fintech_school.currency_trader.data.Currency;
import com.fintech_school.currency_trader.parents.adapters.ClickListener;

public interface OnCurrencyClickListener extends ClickListener{

    void onCurrencyClick(Currency currency);
    boolean onLongCurrencyClick(Currency currency, int position);
    void onFavoriteMarkClick(Currency currency);
}
