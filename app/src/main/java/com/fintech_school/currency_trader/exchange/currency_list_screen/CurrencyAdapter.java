package com.fintech_school.currency_trader.exchange.currency_list_screen;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fintech_school.currency_trader.R;
import com.fintech_school.currency_trader.data.Currency;
import com.fintech_school.currency_trader.databinding.CurrencyItemBinding;
import com.fintech_school.currency_trader.parents.adapters.BaseMutableAdapter;
import com.fintech_school.currency_trader.parents.adapters.ClickListener;

public class CurrencyAdapter extends BaseMutableAdapter<Currency> {

    public CurrencyAdapter(@Nullable ClickListener listener) {
        super(listener);
    }

    @Override
    protected boolean areItemsTheSame(Currency first, Currency second) {
        return first.equals(second);
    }

    @Override
    protected boolean areContentsTheSame(Currency first, Currency second) {
        return first.isFavorite() == second.isFavorite();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CurrencyItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.currency_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bind(elements.get(position), position, (OnCurrencyClickListener) listener);
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        private CurrencyItemBinding binding;

        private ViewHolder(CurrencyItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bind(Currency currency, int position, OnCurrencyClickListener listener) {
            binding.setCurrency(currency);
            binding.getRoot().setOnClickListener((view) -> listener.onCurrencyClick(currency));
            binding.getRoot().setOnLongClickListener((view) -> listener.onLongCurrencyClick(currency, position));
            binding.mark.setOnClickListener((view) -> listener.onFavoriteMarkClick(currency));
        }
    }
}