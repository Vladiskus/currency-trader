package com.fintech_school.currency_trader.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.fintech_school.currency_trader.util.DateUtil;

import java.util.Date;

@Entity
public class HistoricalData implements Comparable<HistoricalData> {

    @PrimaryKey
    @NonNull private String id;
    private String currency;
    private Date date;
    private double value;

    public HistoricalData(String currency, Date date, double value) {
        this.currency = currency;
        this.date = date;
        this.value = value;
        id = currency + DateUtil.getString(date, "yyyy-MM-dd");
    }

    public void setDate(Date date) {
        this.date = date;
        id = currency + DateUtil.getString(date, "yyyy-MM-dd");
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public Date getDate() {
        return date;
    }

    public double getValue() {
        return value;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HistoricalData that = (HistoricalData) o;
        return currency.equals(that.currency) && date.equals(that.date);
    }

    @Override
    public int hashCode() {
        int result = currency.hashCode();
        result = 31 * result + date.hashCode();
        return result;
    }

    @Override
    public int compareTo(@NonNull HistoricalData o) {
        return (int) (date.getTime() - o.date.getTime());
    }
}