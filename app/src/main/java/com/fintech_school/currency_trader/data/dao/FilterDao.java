package com.fintech_school.currency_trader.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.fintech_school.currency_trader.data.Filter;

import io.reactivex.Maybe;

@Dao
public interface FilterDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveFilter(Filter filter);

    @Query("SELECT * FROM filter WHERE id = 1")
    Maybe<Filter> getFilter();
}
