package com.fintech_school.currency_trader.parents.adapters;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseMutableAdapter<T> extends BaseAdapter<T> {

    public BaseMutableAdapter(@Nullable ClickListener listener) {
        super(null, listener);
    }

    public void setElements(List<T> newElements, @Nullable Runnable executeLast) {
        if (elements == null) {
            elements = newElements;
            notifyItemRangeInserted(0, newElements.size());
        } else Single.fromCallable(createCallable(new WeakReference<>(this), newElements))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(diffResult -> {
                    elements = newElements;
                    diffResult.dispatchUpdatesTo(this);
                    if (executeLast != null) executeLast.run();
                }, Throwable::printStackTrace);
    }

    private static Callable<DiffUtil.DiffResult> createCallable(WeakReference<BaseMutableAdapter> adapter,
                                                                List newList) {
        return () -> DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return adapter.get().elements.size();
            }

            @Override
            public int getNewListSize() {
                return newList.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                return adapter.get().areItemsTheSame(adapter.get().elements.get(oldItemPosition),
                        newList.get(newItemPosition));
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                return adapter.get().areContentsTheSame(adapter.get().elements.get(oldItemPosition),
                        newList.get(newItemPosition));
            }
        });
    }

    protected abstract boolean areItemsTheSame(T first, T second);

    protected boolean areContentsTheSame(T first, T second) {
        return first.equals(second);
    }
}