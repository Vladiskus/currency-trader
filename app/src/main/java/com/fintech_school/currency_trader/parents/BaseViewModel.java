package com.fintech_school.currency_trader.parents;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.fintech_school.currency_trader.main.MyApp;
import com.fintech_school.currency_trader.repo.MainDataSource;
import com.fintech_school.currency_trader.repo.interfaces.CurrencyDataSource;
import com.fintech_school.currency_trader.repo.interfaces.FilterDataSource;
import com.fintech_school.currency_trader.repo.interfaces.HistoricalDataSource;
import com.fintech_school.currency_trader.repo.interfaces.TransactionDataSource;

import javax.inject.Inject;

public abstract class BaseViewModel extends AndroidViewModel {

    @Inject MainDataSource mainDataSource;

    public BaseViewModel(@NonNull Application application) {
        super(application);
        ((MyApp) application).getAppComponent().inject(this);
    }

    protected CurrencyDataSource getCurrencyDataSource() {
        return mainDataSource;
    }

    protected FilterDataSource getFilterDataSource() {
        return mainDataSource;
    }

    protected HistoricalDataSource getHistoricalDataSource() {
        return mainDataSource;
    }

    protected TransactionDataSource getTransactionDataSource() {
        return mainDataSource;
    }
}