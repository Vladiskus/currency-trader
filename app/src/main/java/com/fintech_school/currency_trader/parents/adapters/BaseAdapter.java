package com.fintech_school.currency_trader.parents.adapters;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected List<T> elements = new ArrayList<>();
    protected ClickListener listener;

    public BaseAdapter(@Nullable List<T> elements, @Nullable ClickListener listener) {
        if (elements != null) this.elements = elements;
        this.listener = listener;
    }

    public void addElement(T element, int position) {
        elements.add(position, element);
        notifyItemInserted(position);
    }

    public void removeElement(T element) {
        if (!elements.contains(element)) return;
        int position = elements.indexOf(element);
        elements.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return elements.size();
    }
}