package com.fintech_school.currency_trader.util;

import android.arch.persistence.room.TypeConverter;

import com.fintech_school.currency_trader.data.Filter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Date;
import java.util.List;

public class Converters {

    @TypeConverter
    public static Long getLong(Date date) {
        return date.getTime();
    }

    @TypeConverter
    public static Date getDate(Long time) {
        return new Date(time);
    }

    @TypeConverter
    public static List<String> getList(String value) {
        return new Gson().fromJson(value, new TypeToken<List<String>>() {}.getType());
    }

    @TypeConverter
    public static String getString(List<String> list) {
        return new Gson().toJson(list);
    }

    @TypeConverter
    public static String getString(Filter.Period period) {
        return period == null ? "" : period.name();
    }

    @TypeConverter
    public static Filter.Period getPeriod(String period) {
        return period.equals("") ? null : Filter.Period.valueOf(period);
    }
}